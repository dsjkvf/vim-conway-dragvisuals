# Dragging visual blocks

## About

This is a Vim plugin for moving lines (or blocks) of text around easily. Originally written by [Damian Conway](http://damian.conway.org/About_us/Bio_formal.html), copied now from [Damian's collection](https://github.com/thoughtstream/Damian-Conway-s-Vim-Setup) of his Vim-related files to a separate repository in order to make the installation process easier.

## Configuration

Add the following to one's `.vimrc`:

    vmap <expr> <S-LEFT> DVB_Drag('left')
    vmap <expr> <S-RIGHT> DVB_Drag('right')
    vmap <expr> <S-DOWN> DVB_Drag('down')
    vmap <expr> <S-UP> DVB_Drag('up')

## Appendix

As an alternative, one may be also interested in [vim-schlepp](https://github.com/zirrostig/vim-schlepp) plugin.
